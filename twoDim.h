//Goldmine project
//CPSC340 - Scott Summerford (12 - 1:50)
//Header for twoDim.cpp, provided by professor.

#ifndef TWODIMH
#define TWODIMH
template <class LE>
class twoDim
{
  public:
        twoDim();
        LE getElement(int row, int col) const;
        void setElement (int row, int col, LE element);
   private:
        LE	array[6][6];
};

#endif