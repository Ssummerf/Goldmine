//Goldmine project
//CPSC340 - Scott Summerford (12 - 1:50)

#include <iostream>
#include "GoldMine.h"
#include "twoDim.h"

using namespace std;

int main(){
	GoldMine runner;
	//Placeholders for input extraction
	string in1 = "40";
	string in2 = "40";
	int inputone = 30; 
	int inputtwo = 30;
	bool notbreaking = true;
	bool playagain = true;
	bool CHEATER = false;
	char yesno = 'A';

	//Running loop
	while(playagain){
	cout << "Welcome to the Goldmine game." << endl;

	runner.printGoldMine();
	cout << endl;

	cout << "To end the game, enter 99 for any input selection." << endl;
	cout << "To cheat and reveal the board, enter 999 for any input selection." << endl;


	//Main Game loop. Runs under a couple conditions
	/** First, all inputs must be within array range
	* Second, all inputs must be integers
	* Third, if either input is 99, the game stops (Quit command)
	* Lastly, if the Gold or Coal are found, the game ends.
	*/
	do{

		//First input loop, checks for non-integer characters, and outside array values.
		do{
		cout << "Please enter the row you want to search (1 - 6): ";

		while(!(cin >> inputone)){
    	cin.clear();
    	cin.ignore(numeric_limits<streamsize>::max(), '\n');

    	cout <<"Row must be an integer!"<< endl;
    	cout << "Please enter the row you want to search (1 - 6): ";
		}

		}while((inputone > 6 || inputone < 1) && inputone!= 99 && inputone!= 999);

		//Second input loop, checks for non-integer characters, and outside array values.
		if(inputone != 99 && inputone != 999){
		do{

		cout << "Please enter the column you want to search (1 - 6): ";
		while(!(cin >> inputtwo)){
    	cin.clear();
    	cin.ignore(numeric_limits<streamsize>::max(), '\n');

    	cout <<"Column must be an integer!"<< endl;
    	cout << "Please enter the column you want to search (1 - 6): ";
		}

		}while((inputtwo > 6 || inputtwo < 1) && inputtwo!= 99 && inputtwo != 999);
		}

		// If either input is 99, the breaking flag is triggered here.
		if(inputone == 99 || inputtwo == 99)
			notbreaking = false;

		if(inputone == 999 || inputtwo == 999)
			CHEATER = true;

		if(notbreaking && !CHEATER){
		//Revealing what's placed on the viable row/columns.
		runner.revealSelection(inputone-1,inputtwo-1);


		runner.printGoldMine();
	    cout << endl;
		}
		
		if(CHEATER){
			runner.revealSelection(inputone, inputtwo);
			runner.printGoldMine();
			cout << endl;
			CHEATER = false;
		}


	}while(runner.getGold() > 0 && runner.getGold() < 10000 && notbreaking);

	// Conditional ending responses.
	if(runner.getGold() <= 0)
		cout << "GAME OVER" << endl;
	if(runner.getGold() > 10000)
		cout << "Victory!" << endl;
	if(!notbreaking){
		cout << "Goodbye!" << endl;
		playagain = false;
	}

	//If we didn't force out, offer the chance to play again.
	if(notbreaking){
	cout << "Would you like to play again? (y/n)" << endl;
	cin >> yesno;
	if(yesno == 'y' || yesno == 'Y'){
		playagain = true;
		runner = GoldMine();
	}
	else
		playagain = false;
	}
	}

	return 0;
}