//Goldmine project
//CPSC340 - Scott Summerford (12 - 1:50)
//Header for goldmine.cpp, provided by professor.

#ifndef GOLDMINEH
#define GOLDMINEH
#include "twoDim.h"
using namespace std;

class GoldMine
{
  public:
        GoldMine();
	    void resetGame();
        void randomRowColumn(int &row, int &column);
        string rainbowMessage();
        void revealSelection(int row, int column);
        void printGoldMine();
        int getGold(){return gold;};
   private:
        twoDim<bool> visited; //creates the array of boxes
	    twoDim<char> boxType;
	    twoDim<int>  goldAmount;
        int gold; // holds the initial amount of gold 1000 - 9000
};

#endif