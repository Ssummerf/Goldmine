//Goldmine project
//CPSC340 - Scott Summerford (12 - 1:50)


#include <iostream>
#include <cstdlib>
#include "GoldMine.h"
#include "twoDim.cpp"

using namespace std;

//Resets the Game, initializes the Gold, Coal, and Rainbow locations
GoldMine::GoldMine(){
	resetGame();
	srand (time (NULL));

	//Variable declaration for positioning.
	int row1, row2, row3;
	int col1, col2, col3;
	int pos1, pos2, pos3;

	//Randomizing the three objects positions. Continues to loop until they all have unique locations.
	randomRowColumn(row1, col1);
	pos1 = (row1 + 1) * 6 + col1; 
	do{
		randomRowColumn(row3, col3);
		randomRowColumn(row2, col2);
		pos2 = (row2 + 1) * 6 + col2;
		pos3 = (row3 + 1) * 6 + col3;
	}while((pos2 == pos1 || pos2 == pos3) || (pos3 == pos1 || pos3 == pos2) );

	boxType.setElement(row1,col1,'G');
	boxType.setElement(row2,col2,'R');
	boxType.setElement(row3,col3,'C');
}

//Initializes Gold and arrays
void GoldMine::resetGame(){
	this->gold = rand() % 8000 + 1000;

	for(int i = 0; i < 6; i++){
		for(int j = 0; j < 6; j++){
			visited.setElement(i,j,false);
			boxType.setElement(i,j,'-');
			goldAmount.setElement(i,j,0);
		}
	}
}

//Randomizes rows/columns using pass by
void GoldMine::randomRowColumn(int &row, int &column){
	row = rand() % 5;
	column = rand() % 5;
}

//If the rainbow object is found, loop through the array, find the Gold, and give a hint based on location
string GoldMine::rainbowMessage(){
	string updown = "down ";
	string leftright = "right";
	bool upper = false;
	bool left = false;

	for(int i = 0; i < 6; i++){
		for(int j = 0; j < 6; j++){
			if(boxType.getElement(i,j) == 'G' && i < 3)
				upper = true;
			if(boxType.getElement(i,j) == 'G' && j < 3)
				left = true;
		}
	}

	if(upper)
		updown = "upper ";
	if(left)
		leftright = "left";

	string returner = "Check the " + updown + leftright + " corner.";
	return returner;
}

//Switch - case for what's at a position.
void GoldMine::revealSelection(int row, int column){


	//Cheater function
	if(row == 999 || column == 999){
		for(int i = 0; i < 6; i++){
		for(int j = 0; j < 6; j++){
			visited.setElement(i,j,true);
		}

	}
	}else{
	char placeholder = boxType.getElement(row,column);
	visited.setElement(row,column,true);
	int loss = rand() % 200 + 100;

	//If not cheating, do the regular placements.
	switch(placeholder){
		case '-':
			this->gold -= loss;  
			cout << "You didn\'t find anything, but while you were looking, you lost some gold!" << endl;
			cout << "Gold lost: " << loss << endl;
			cout << "Gold left: " << this->gold << endl;
			break;
		case 'C':
			cout << "You found coal. Unfortunately, you lose." << endl;
			this->gold = 0;
			break;
		case 'G':
			cout << "You found the pot of gold. You win!" << endl;
			cout << "Your gold: " << this->gold << endl;
			this->gold = 100000;
			break;
		case 'R': 
			cout << "You found the rainbow!" << endl;
			cout << rainbowMessage() << endl;
			cout << "Your gold: " << this->gold << endl;
	}
	}
}

//Printing Game Board.
void GoldMine::printGoldMine(){
	cout << "   1 2 3 4 5 6" << endl;

	for(int i = 0; i < 6; i++){
		cout << ' ' << i+1 << ' ';

		for(int j = 0; j < 6; j++){
			if(visited.getElement(i,j)){
				cout << boxType.getElement(i,j) << " ";
			}else{
				cout << "* ";
			}
		}

		cout << endl;
	}

}
