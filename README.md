# C++ Goldmine Text Game

A text based game focused on using abstraction to build a grid, and then letting the players play a sort of guessing game.

## TwoDim

Our abstract space that fills the grid. It knows it's location and its hidden type.

## Goldmine

The main method dump of the program. It builds the grid, randomizes the position, and handles the changes to the game state based on the users input.

## Main

Handles the input / output, as well as ensuring that all inputs are valid relative to the array and to the commands available.

## Ending Remarks

The code of this program was developed by Scott Summerford. The project outline and description were provided by Jennifer Polack