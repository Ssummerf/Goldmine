//Goldmine project
//CPSC340 - Scott Summerford (12 - 1:50)

//Honestly this is just getters / setters for array objects. Nothing much to explain.

#include <iostream>
#include "twoDim.h"

using namespace std;

template<class LE>
twoDim<LE>::twoDim(){
	for(auto& rows: array){
    for(auto& elem: rows)
    {
        elem = 0;
    }
}
}

template<class LE>
LE twoDim<LE>::getElement(int row, int col)const{
	return array[row][col];
}

template<class LE>
void twoDim<LE>::setElement(int row, int col, LE element){
	array[row][col] = element;
}
